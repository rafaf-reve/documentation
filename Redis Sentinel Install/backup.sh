#!/bin/bash

# Backing Up Conf files


## Master 
scp root@178.79.137.179:/etc/redis.conf 178.79.137.179/etc/redis.conf
scp root@178.79.137.179:/etc/redis-sentinel.conf 178.79.137.179/etc/redis-sentinel.conf

## Sentinel 1
scp root@172.105.58.148:/etc/redis/redis.conf 172.105.58.148/etc/redis/redis.conf
scp root@172.105.58.148:/etc/redis/sentinel.conf 172.105.58.148/etc/redis/sentinel.conf

## Sentinel 2
scp root@172.105.57.21:/etc/redis/redis.conf 172.105.57.21/etc/redis/redis.conf
scp root@172.105.57.21:/etc/redis/sentinel.conf 172.105.57.21/etc/redis/sentinel.conf

