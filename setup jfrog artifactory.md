How to install jfrog artifactory in a CentOS machine 
======================================

Install jFrog
-----------
Installing jFrog is pretty straight forward as described in the documentation : https://jfrog.com/open-source/

```
wget https://releases.jfrog.io/artifactory/artifactory-rpms/artifactory-rpms.repo -O jfrog-artifactory-rpms.repo;
sudo mv jfrog-artifactory-rpms.repo /etc/yum.repos.d/;
sudo yum update && sudo yum install jfrog-artifactory-oss
```

start artifactory service: 

```
systemctl start artifactory.service
systemctl status artifactory
systemctl enable artifactory
```

This will run jfrog on port 8082, access it with <jfrog-url>:8082

Whitelist port in firewall
-----------------------

If firewall is enabled, we need to open port. check if firewalld is running 

```
systemctl status firewalld
```

If it's enabled, open port 8082, 80 and 443.

```
firewall-cmd --zone=public --permanent --add-port=8082/tcp
firewall-cmd --zone=public --permanent --add-port=443/tcp
firewall-cmd --zone=public --permanent --add-port=80/tcp
```
To know more in details: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-7

Install nginx
------------

```
yum install epel-release
yum install nginx
systemctl start nginx
systemctl status nginx
systemctl enable nginx
```

Now we need to update nginx config so that it proxy_pass jFrog artifactory (8082) port to port 80

Add file `/etc/nginx/conf.d/jfrong-artifactory.conf ` with content 

```
server {
   listen         80;
   server_name    jfrog-artifact.revechat.com;

   location / {
      proxy_pass  http://127.0.0.1:8082;
   }
}
```

Make sure to enter proper ip in dns entry for jfrog-artifact.revechat.com . If everything is fine, jFrog should be up and running and can be accessed from jfrog-artifact.revechat.com. 

Note: sometime theres some error in nginx accessing to a 80 port. If you face the problem, you need to do 

```
setsebool -P httpd_can_network_connect 1
```

More in detail: https://stackoverflow.com/questions/25235453/nginx-proxy-server-localhost-permission-denied

Setting up SSL
--------------

```
sudo wget -r --no-parent -A 'epel-release-*.rpm' https://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/e/
sudo rpm -Uvh dl.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-*.rpm
sudo yum-config-manager --enable epel*
sudo yum repolist all

sudo yum install -y certbot python2-certbot-nginx
sudo certbot
```

Everything should be up and running by now ... Go to the dashboard at https://jfrog-artifact.revechat.com and configure things from GUI.


jfrog backup is continuously done at - /jfrog-backup

jfrog need firewall to be permanently off