## List of Services running in production


| Service               | Endpoint                                                                                  | IP Address         |
| --------------------- | ------------------------------------------------------------------------------------------|--------------------|
| ML-Services           | https://ai.revechat.com/health/check                                                      | 139.162.170.85     |
| Chat-services         | https://chat-socket.revechat.com/health/check , wss://chat-socket.revechat.com/x/y/z/     | 104.26.9.127       |
| dashboard             | https://dashboard.revechat.com , https://app.revechat.com                                 | 139.162.154.104    |
| social media gateway  | https://social-gateway.revechat.com/                                                      | 104.26.8.127       |
| file-services         | https://file.revechat.com/agent/pro-pic/7871954-e71bbe33-9b31-4f0c-bb5f-1c2fb6a6b77e.png  | 172.67.73.115      |
| analytics             | https://analytics.revechat.com/                                                           | 139.162.170.85     |
| revechat website      | https://revechat.com                                                                      | 172.67.73.115      |
| co-browsing-services  | https://cobrowsing-dev-proxy.revechat.com:7443/                                           | 139.162.154.104    |