#!/bin/bash

scp root@172.104.170.147:/etc/systemd/system/bkash-ml-service-venv-v2.service etc/systemd/system/bkash-ml-service-venv-v2.service
scp root@172.104.170.147:/etc/systemd/system/demo-app.service etc/systemd/system/demo-app.service
scp root@172.104.170.147:/etc/systemd/system/smalltalk-service.service etc/systemd/system/smalltalk-service.service