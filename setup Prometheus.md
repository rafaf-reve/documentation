# Master 

## Install node_exporter 
```
cd /opt
wget https://github.com/prometheus/node_exporter/releases/download/v1.2.2/node_exporter-1.2.2.linux-amd64.tar.gz
tar -xzvf node_exporter-1.2.2.linux-amd64.tar.gz 
cp -r node_exporter-1.2.2.linux-amd64 node_exporter
rm -rf node_exporter-1.2.2.linux-amd64*
cd /opt/node_exporter/

tee node_exporter.service <<EOF
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/opt/node_exporter/node_exporter
WorkingDirectory=/opt/node_exporter/

[Install]
WantedBy=multi-user.target
EOF

chmod +x node_exporter.service
ln -s /opt/node_exporter/node_exporter.service /etc/systemd/system/node_exporter.service
systemctl status node_exporter
systemctl start node_exporter
systemctl enable node_exporter

```

## Install black box exporter 

```
cd /opt

wget https://github.com/prometheus/blackbox_exporter/releases/download/v0.19.0/blackbox_exporter-0.19.0.linux-amd64.tar.gz
tar -xzvf blackbox_exporter-0.19.0.linux-amd64.tar.gz
cp -r blackbox_exporter-0.19.0.linux-amd64 blackbox_exporter
rm -rf blackbox_exporter-0.19.0.linux-amd64*

cd blackbox_exporter
cp blackbox.yml blackbox.yml.orig

tee blackbox.yml <<EOF
modules:
  http_2xx:
    prober: http
    timeout: 5s
    http:
      valid_http_versions: ["HTTP/1.1", "HTTP/2.0"]
      method: GET
      no_follow_redirects: false
      tls_config:
        insecure_skip_verify: true
      preferred_ip_protocol: "ip4"
      ip_protocol_fallback: false
EOF


tee blackbox.service <<EOF
[Unit]
Description=BlackBox
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/opt/blackbox_exporter/blackbox_exporter --config.file=/opt/blackbox_exporter/blackbox.yml
WorkingDirectory=/opt/blackbox_exporter

[Install]
WantedBy=multi-user.target
EOF

ln -s /opt/blackbox_exporter/blackbox.service /etc/systemd/system/blackbox.service

systemctl start blackbox
systemctl status blackbox
systemctl enable blackbox

```

## Install Prometheus 

```
cd /opt

wget https://github.com/prometheus/prometheus/releases/download/v2.29.2/prometheus-2.29.2.linux-amd64.tar.gz
tar -xzvf prometheus-2.29.2.linux-amd64.tar.gz
cp -r prometheus-2.29.2.linux-amd64 prometheus
rm prometheus-2.29.2.linux-amd64.tar.gz 
rm -rf prometheus-2.29.2.linux-amd64


cp prometheus.yml prometheus.yml.orig

tee prometheus.yml <<EOF
global:
  scrape_interval: 15s 
  evaluation_interval: 15s 

alerting:
  alertmanagers:
    - static_configs:
        - targets:
          # - alertmanager:9093

rule_files:
  - 'prometheus.rules.yml'

scrape_configs:
  - job_name: "prometheus"
    scrape_interval: 5s
    static_configs:
      - targets: ["localhost:9090"]

  - job_name:       'node'

    scrape_interval: 5s

    static_configs:
      - targets: ['localhost:9100']
        labels:
          group: 'canary'

  - job_name: 'blackbox'
    metrics_path: /probe
    params:
      module: [http_2xx]
    static_configs:
      - targets:
        - https://ai.revechat.com/health/check
        - https://chat-socket.revechat.com/health/check 
        - https://dashboard.revechat.com
        - https://social-gateway.revechat.com
        - https://analytics.revechat.com
        - https://revechat.com
        - https://cobrowsing-dev-proxy.revechat.com:7443
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9115  # The blackbox exporter's real hostname:port.

EOF


tee prometheus.service <<EOF
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/opt/prometheus/prometheus --config.file=/opt/prometheus/prometheus.yml
WorkingDirectory=/opt/prometheus

[Install]
WantedBy=multi-user.target
EOF


ln -s /opt/prometheus/prometheus.service /etc/systemd/system/prometheus.service
systemctl start prometheus
systemctl status prometheus
systemctl enable prometheus 
```

## Install Grafana

```
cd /opt
wget https://dl.grafana.com/enterprise/release/grafana-enterprise-8.1.3.linux-amd64.tar.gz
tar -xzvf grafana-enterprise-8.1.3.linux-amd64.tar.gz
cp -r grafana-8.1.3 grafana
rm -rf grafana-*
cd grafana/

tee grafana.service <<EOF
[Unit]
Description=Grafana
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/opt/grafana/bin/grafana-server
WorkingDirectory=/opt/grafana/bin

[Install]
WantedBy=multi-user.target
EOF

ln -s /opt/grafana/grafana.service /etc/systemd/system/grafana.service
systemctl status grafana
systemctl start grafana
systemctl enable grafana


tee grafana.conf <<EOF
server {
   server_name    monitor.revechat.com;
    listen         80;

   location / {
      proxy_pass  http://127.0.0.1:3000;
   }
}
EOF

ln -s /opt/grafana/grafana.conf /etc/nginx/conf.d/grafana.conf 

nginx -t
systemctl restart nginx 

certbot

firewall-cmd --zone=public --permanent --add-port=443/tcp
firewall-cmd --zone=public --permanent --add-port=80/tcp

cp defaults.ini defaults.ini.orig
```
