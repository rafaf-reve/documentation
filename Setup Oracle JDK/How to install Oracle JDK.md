[How to install Oracle JDK](https://docs.datastax.com/en/jdk-install/doc/jdk-install/installOracleJdkDeb.html)


mkdir -p /usr/lib/jvm
tar zxvf jdk-version-linux-x64.tar.gz -C /usr/lib/jvm
update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_version/bin/java" 1
update-alternatives --set java /usr/lib/jvm/jdk1.8.0_version/bin/java


