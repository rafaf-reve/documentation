NFS Master : 109.74.203.44
---------------------------
```
yum update
yum install nfs-utils


systemctl enable rpcbind
systemctl enable nfs-server
systemctl enable nfs-lock
systemctl enable nfs-idmap
systemctl start rpcbind
systemctl start nfs-server
systemctl start nfs-lock
systemctl start nfs-idmap
```

Configure `/etc/exports`

```
[root@li150-44 ~]# cat /etc/exports
/mnt/revechat-file-storage		139.162.154.104(rw,sync,no_root_squash,no_subtree_check)
```

NFS Client: 139.162.154.104
-------------------------
Install `nfs-utils`

```
yum install nfs-utils
```

Unmount or create folder directory to be mounted.

```
umount /mnt/revechat-file-storage-fr
```

Mount direcotry 

```
mount -t nfs 109.74.203.44:/mnt/revechat-file-storage /mnt/revechat-file-storage-fr
```

Add the following line in `/etc/fstab` to automount after reboot 

```
109.74.203.44:/mnt/revechat-file-storage /mnt/revechat-file-storage-fr nfs defaults 0 0

```