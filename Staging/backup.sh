#!/bin/bash

scp root@172.105.41.74:/etc/systemd/system/chat-server-1.service etc/systemd/system/chat-server-1.service
scp root@172.105.41.74:/etc/systemd/system/chat-server-2.service etc/systemd/system/chat-server-2.service
scp root@172.105.41.74:/etc/systemd/system/social-gateway.service etc/systemd/system/social-gateway.service
scp root@172.105.41.74:/etc/systemd/system/file-upload-server.service etc/systemd/system/file-upload-server.service
scp root@172.105.41.74:/etc/systemd/system/ml-service.service etc/systemd/system/ml-service.service
scp root@172.105.41.74:/etc/systemd/system/smalltalk-service.service etc/systemd/system/smalltalk-service.service
scp root@172.105.41.74:/etc/systemd/system/dashboard.service etc/systemd/system/dashboard.service
