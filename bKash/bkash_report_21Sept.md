I would like to seek help regarding the following problem ... 

Following is the proredure to install ML service in a centos box 

```
# Installing gcc dependency - https://github.com/facebookresearch/fastText/issues/1105#issuecomment-667041482
yum install centos-release-scl
yum install scl-utils-build
yum install devtoolset-9
scl enable devtoolset-9 bash


# Downloading fasttext pre-built model
yum install wget
wget https://github.com/facebookresearch/fastText/archive/v0.9.2.zip
unzip v0.9.2.zip
cd fastText-0.9.2
make
cd ..

# Language detection Model
cd mlmodules/services/languagedetect/resources
wget https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.bin
cd ../../../..

python3.6 -m venv venv 
source venv/bin/activate
pip install -U pip 

pip install -r requirements.txt
python -m nltk.downloader popular
```

What I currently need to do ... 

1. If I enable devtoolset-9 manually with `scl enable devtoolset-9 bash`, it works fine, but I need to make it default buildtool. Any suggestion how can I do this ? 

2. I need python36-devel installed in the staging server. So that default python gets it. Currently I don't see any python36-devel package. I only find the following ... 