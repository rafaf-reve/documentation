yum update
yum install python3 python3-pip python3-devel



git clone --single-branch -b gridsearch-and-data-augmentation https://rafaf-reve@bitbucket.org/reve_chat/bkash-ml-service-latest.git



# Installing gcc dependency - https://github.com/facebookresearch/fastText/issues/1105#issuecomment-667041482
yum install centos-release-scl
yum install scl-utils-build
yum install devtoolset-9
scl enable devtoolset-9 bash


# Downloading fasttext pre-built model
yum install wget
wget https://github.com/facebookresearch/fastText/archive/v0.9.2.zip
unzip v0.9.2.zip
cd fastText-0.9.2
make
cd ..

# Language detection Model
cd mlmodules/services/languagedetect/resources
wget https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.bin
cd ../../../..

python3.6 -m venv venv 
source venv/bin/activate
pip install -U pip 

pip install -r requirements.txt
python -m nltk.downloader popular